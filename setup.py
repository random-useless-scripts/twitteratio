from setuptools import setup, find_packages
setup(
	name = "twitteRatio",
	version = "1.0",
	packages = find_packages(),
	scripts = ['twitteRatios.py'],
	install_requires = ['tweepy'],
	author = "Ben Cordero",
	author_email = "bmc@linux.com",
	description = "Find out twitter follower/followee ratios",
	keywords = "twitter follower ratio",
	url = "https://gitorious.org/random-useless-scripts/twitteratio"
)
