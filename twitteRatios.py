import sys
import tweepy

try:
	user = sys.argv[1]
except:
	print("Enter a twitter username or id")
	user = raw_input()

try:
	u = tweepy.api.get_user(user)
except:
	print ("username '%s' cannot be found"%user)
	exit()

followers = u.followers_count
friends = u.friends_count
name = u.name

try:
	ratio=float(followers)/float(friends)
except ZeroDivisionError:
	ratio=-followers
print "%s has a follower ratio of %0.2f%%" % (name, ratio)